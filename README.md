# Ideapool

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.1.1.

# Firebase

Run `npm run build` to build the project
Run `firebase deploy` to deploy the project.
Run `firebase deploy --only functions` to deploy only the functions.

Navigate to `https://codementorx-73410.firebaseapp.com/` to view the app.

# API Code

This is a firebase app. So all the server code is in the functions folder. Firebase.json contains all the options for firebase. This code is uploaded to firebase functions.

The angular code compiles AOT to the dist folder. This folder is uploaded to firebase hosting.

The database used for storing ideas, user information and refresh tokens is firestore.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
