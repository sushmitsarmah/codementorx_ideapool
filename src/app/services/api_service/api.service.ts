import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { Observable, Subscription } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';

import { catchError } from 'rxjs/operators';

import {
    LoginParams,
    SignupParams,
    Idea,
    IdeaParams,
    AccessTokenServerResponse,
    User
} from '../../interfaces';

import { Store } from '@ngrx/store';
import * as fromRoot from '../../ngrx/reducers';
import * as authActions from '../../ngrx/actions/auth';

import { environment } from '../../../environments/environment';

import {
    Router
} from '@angular/router';
import { isNull } from '@angular/compiler/src/output/output_ast';

@Injectable({
    providedIn: 'root'
})
export class ApiService {

    private APIURL = environment.apiUrl;

    constructor(
        private http: HttpClient,
        private jwtHelper: JwtHelperService,
        private router: Router,
        private store: Store<fromRoot.State>
    ) {
    }

    // get the authorization token
    public getAuthorizationToken(): string {
        return localStorage.getItem('cmx_access_token');
    }

    // get the refresh token
    public getRefreshToken(): string {
        return localStorage.getItem('cmx_refresh_token');
    }

    // delete all tokens. after logout.
    public deleteTokens() {
        localStorage.removeItem('cmx_access_token');
        localStorage.removeItem('cmx_refresh_token');
    }

    // save tokens to localstorage
    public saveTokens(tokens: AccessTokenServerResponse): User {
        const user: User = this.jwtHelper.decodeToken(tokens.jwt);
        localStorage.setItem('cmx_access_token', <string>tokens.jwt);
        localStorage.setItem('cmx_refresh_token', <string>tokens.refresh_token);
        return user;
    }

    // get the current logged in user
    public getCurrentUser(): Observable<User> {
        return this.http.get<User>(`${this.APIURL}/me`);
    }

    // get access token and refresh token. used for login.
    public getAccessToken(data: LoginParams): Observable<AccessTokenServerResponse> {
        return this.http.post<AccessTokenServerResponse>(`${this.APIURL}/access-tokens`, data);
    }

    // refresh access token. expires in 10 minutes. refresh token is required.
    public refreshAccessToken(): Observable<AccessTokenServerResponse> {
        const data = {
            refresh_token: this.getRefreshToken()
        };
        const url = `${this.APIURL}/access-tokens/refresh`;

        return this.http.post<AccessTokenServerResponse>(url, data);
    }

    // delete access token. used for logout
    public deleteAccessToken(): Observable<object> {
        const refresh_token = this.getRefreshToken();
        const url = `${this.APIURL}/access-tokens?refresh_token=${refresh_token}`;

        return this.http.request('delete', url, { body: { refresh_token: refresh_token } });
    }

    public logout() {
        const refresh_token = this.getRefreshToken();
        if (refresh_token) {
            const subscription = this.deleteAccessToken()
            .pipe(catchError( error => {
                console.log(error);
                subscription.unsubscribe();
                this.deleteTokens();
                this.store.dispatch(new authActions.Logout());
                this.router.navigate(['login']);
                return error;
            }))
            .subscribe(d => {
                subscription.unsubscribe();
                this.deleteTokens();
                this.store.dispatch(new authActions.Logout());
                this.router.navigate(['login']);
            });
        } else {
            this.deleteTokens();
            this.store.dispatch(new authActions.Logout());
            this.router.navigate(['login']);
        }
    }

    // get a list of ideas. 1 page = 10 ideas
    public getIdeas(page: number): Observable<Idea[]> {
        return this.http.get<Idea[]>(`${this.APIURL}/ideas?page=${page}`);
    }

    // get a list of ideas. 1 page = 10 ideas
    public getIdeasCount(): Observable<object> {
        return this.http.get<object>(`${this.APIURL}/total-ideas`);
    }

    // create an idea
    public createIdea(data: IdeaParams): Observable<Idea> {
        return this.http.post<Idea>(`${this.APIURL}/ideas`, data);
    }

    // delete an idea
    public deleteIdea(id: string): Observable<object> {
        return this.http.delete(`${this.APIURL}/ideas/${id}`);
    }

    // update an idea
    public updateIdea(id: string, idea: IdeaParams): Observable<Idea> {
        const data = {
            content: idea.content,
            impact: idea.impact,
            ease: idea.ease,
            confidence: idea.confidence,
            average_score: idea.average_score
        };

        return this.http.put<Idea>(`${this.APIURL}/ideas/${id}`, data);
    }

    // signup new users
    public userSignup(data: SignupParams): Observable<AccessTokenServerResponse> {
        return this.http.post<AccessTokenServerResponse>(`${this.APIURL}/users`, data);
    }

}
