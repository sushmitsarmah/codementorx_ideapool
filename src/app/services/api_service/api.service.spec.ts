import { TestBed, inject } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';

import { ApiService } from './api.service';

describe('ApiServiceService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
        imports: [HttpClientModule],
        providers: [ApiService]
        });
    });

    it('should be created', inject([ApiService], (service: ApiService) => {
        expect(service).toBeTruthy();
    }));

    it('should have a function getCurrentUser', inject([ApiService], (service: ApiService) => {
        expect(service.getCurrentUser).toBeTruthy();
    }));

    it('should have a function getAccessToken', inject([ApiService], (service: ApiService) => {
        expect(service.getAccessToken).toBeTruthy();
    }));

    it('should have a function refreshAccessToken', inject([ApiService], (service: ApiService) => {
        expect(service.refreshAccessToken).toBeTruthy();
    }));

    it('should have a function deleteAccessToken', inject([ApiService], (service: ApiService) => {
        expect(service.deleteAccessToken).toBeTruthy();
    }));

    it('should have a function getIdeas', inject([ApiService], (service: ApiService) => {
        expect(service.getIdeas).toBeTruthy();
    }));

    it('should have a function createIdea', inject([ApiService], (service: ApiService) => {
        expect(service.createIdea).toBeTruthy();
    }));

    it('should have a function deleteIdea', inject([ApiService], (service: ApiService) => {
        expect(service.deleteIdea).toBeTruthy();
    }));

    it('should have a function updateIdea', inject([ApiService], (service: ApiService) => {
        expect(service.updateIdea).toBeTruthy();
    }));

    it('should have a function userSignup', inject([ApiService], (service: ApiService) => {
        expect(service.userSignup).toBeTruthy();
    }));


});
