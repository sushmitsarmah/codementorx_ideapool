import { Injectable } from '@angular/core';
import {
    HttpInterceptor,
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HTTP_INTERCEPTORS
} from '@angular/common/http';

import { Observable, BehaviorSubject, throwError } from 'rxjs';
import { switchMap, filter, take, catchError } from 'rxjs/operators';

import { ApiService } from './services';
import { environment } from '../environments/environment';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {

    private refreshTokenInProgress = false;
    // Refresh Token Subject tracks the current token, or is null if no token is currently
    // available (e.g. refresh pending).
    private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(
        null
    );

    constructor(
        private apiService: ApiService,
        private jwtHelper: JwtHelperService
    ) { }

    setCustomHeaders(req: HttpRequest<any>) {
        // Get auth token from Local Storage
        const authToken = this.apiService.getAuthorizationToken();

        // If auth token is null this means that user is not logged in
        if (!authToken) {
            return req;
        }

        // cloning request as original request is immutable
        const authReq = req.clone({
            setHeaders: {
                'x-access-token': authToken,
                'Content-Type': 'application/json;charset=utf-8'
            }
        });

        return authReq;
    }

    intercept(
        request: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        const customReq = this.setCustomHeaders(request);
        // replaced request by authreq. check this
        return next.handle(customReq).pipe(
            catchError(error => {
                // We don't want to refresh token for some requests like login or refresh token itself
                // So we verify url and we throw an error if it's the case
                if (
                    request.url.includes('access-tokens/refresh') ||
                    request.url.includes('access-tokens')
                ) {
                    // We do another check to see if refresh token failed
                    // In this case we want to logout user and to redirect it to login page
                    if (request.url.includes('access-tokens/refresh')) {
                        this.apiService.logout();
                    }

                    return throwError(error);
                }

                // If error status is different than 401 we want to skip refresh token
                // So we check that and throw the error if it's the case
                if (error.status !== 401) {
                    return throwError(error);
                }

                if (this.refreshTokenInProgress) {
                    // If refreshTokenInProgress is true, we will wait until refreshTokenSubject has a non-null value
                    // – which means the new token is ready and we can retry the request again
                    return this.refreshTokenSubject
                        .pipe(
                            filter(result => result !== null),
                            take(1),
                            switchMap(() => next.handle(this.setCustomHeaders(request)))
                        );
                } else {
                    this.refreshTokenInProgress = true;

                    // Set the refreshTokenSubject to null so that subsequent API calls will wait until the new token has been retrieved
                    this.refreshTokenSubject.next(null);

                    // Call apiService.refreshAccessToken(this is an Observable that will be returned)
                    return this.apiService
                        .refreshAccessToken().pipe(
                            switchMap((token: any) => {
                                localStorage.setItem('cmx_access_token', token['jwt']);
                                // When refreshToken call completes reset the refreshTokenInProgress to false
                                // for the next time the token needs to be refreshed
                                this.refreshTokenInProgress = false;
                                this.refreshTokenSubject.next(token);

                                return next.handle(this.setCustomHeaders(request));
                            }),
                            catchError((err: any) => {
                                this.refreshTokenInProgress = false;

                                this.apiService.logout();
                                return throwError(error);
                            })
                        );
                }
            })
        );
    }
}

export const httpInterceptorProviders = [
    { provide: HTTP_INTERCEPTORS, useClass: ApiInterceptor, multi: true },
];
