import { Component, OnInit } from '@angular/core';

import { User } from '../../interfaces';

import { ApiService } from '../../services';

import { Store } from '@ngrx/store';
import * as fromRoot from '../../ngrx/reducers';
import * as authActions from '../../ngrx/actions/auth';

import {
    Router
} from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

    public sampleUser: User = {
        name: 'Joyce Lee',
        avatar_url: 'assets/User_ProfilePic@2x.png',
        email: 'joycelee@gmail.com'
    };

    public userDetails: User = <User>{};

    public loggedIn = false;

    constructor(
        private store: Store<fromRoot.State>,
        private apiService: ApiService,
        private router: Router
    ) { }

    ngOnInit() {
        this.store.select(fromRoot.getLoggedIn)
        .subscribe( d => {
            if (d) {
                this.getuser();
            }
        });
    }

    getuser() {
        this.apiService.getCurrentUser()
            .subscribe( (user: User) => {
                this.userDetails = user;
                this.loggedIn = true;
            });
    }

    logout() {
        this.userDetails = <User>{};
        this.loggedIn = false;
        this.apiService.logout();
    }

}
