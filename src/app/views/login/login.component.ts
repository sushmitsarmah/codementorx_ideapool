import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Router } from '@angular/router';

import { ApiService } from '../../services';

import { Store } from '@ngrx/store';
import * as fromRoot from '../../ngrx/reducers';
import * as authActions from '../../ngrx/actions/auth';

import {
    AccessTokenServerResponse,
    User
} from '../../interfaces';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    providers: [ApiService]
})
export class LoginComponent implements OnInit {

    public loginForm: FormGroup;
    public showSpinner = false;
    public showErrorAlert = false;
    public showSuccessAlert = false;

    constructor(
        private formBuilder: FormBuilder,
        private store: Store<fromRoot.State>,
        private apiService: ApiService,
        private router: Router
    ) {
        this.loginForm = this.formBuilder.group({
            email: [
                { value: '', disabled: this.showSpinner },
                [ Validators.required, Validators.email ]
            ],
            password: [
                { value: '', disabled: this.showSpinner },
                Validators.required
            ]
        });
    }

    ngOnInit() {
    }

    showAlert(type: string) {
        if (type === 'success') {
            this.showSuccessAlert = true;
            setTimeout(() => {
                this.showSuccessAlert = false;
            }, 2000);
        } else {
            this.showErrorAlert = true;
            setTimeout(() => {
                this.showErrorAlert = false;
            }, 2000);
        }
    }

    formSubmit() {
        this.showSpinner = true;
        const subscription = this.apiService.getAccessToken(this.loginForm.value)
            .subscribe((tokens: AccessTokenServerResponse) => {
                this.showAlert('success');
                subscription.unsubscribe();
                this.showSpinner = false;
                const user: User = this.apiService.saveTokens(tokens);
                this.store.dispatch(new authActions.LoginSuccess({ user: user }));
                this.loginForm.reset();
                this.router.navigate(['home']);
            }, error => {
                console.log(error);
                this.showAlert('error');
                subscription.unsubscribe();
                this.showSpinner = false;
                this.loginForm.reset();
                this.store.dispatch(new authActions.LoginFailure(error));
            });
    }

}
