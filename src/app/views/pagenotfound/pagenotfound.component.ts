import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pagenotfound',
  templateUrl: './pagenotfound.component.html',
  styleUrls: ['./pagenotfound.component.scss']
})
export class PageNotFoundComponent implements OnInit {

    constructor() { }

    ngOnInit() {
    }

    // attribution required for using this pic.
    gotoFreepik() {
        window.open('https://www.freepik.com/free-vector/404-error-concept-with-laptop_1534899.htm', '_blank');
    }

}
