import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Router } from '@angular/router';

import { ApiService } from '../../services';

import { Store } from '@ngrx/store';
import * as fromRoot from '../../ngrx/reducers';
import * as authActions from '../../ngrx/actions/auth';

import {
    AccessTokenServerResponse,
    User
} from '../../interfaces';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss'],
    providers: [ApiService]
})
export class SignupComponent implements OnInit {

    public signupForm: FormGroup;
    public showSpinner = false;
    public showErrorAlert = false;
    public showSuccessAlert = false;
    public errorMessage = '';

    constructor(
        private formBuilder: FormBuilder,
        private store: Store<fromRoot.State>,
        private apiService: ApiService,
        private router: Router
    ) {
        this.signupForm = this.formBuilder.group({
            name: [
                { value: '', disabled: this.showSpinner },
                Validators.required
            ],
            email: [
                { value: '', disabled: this.showSpinner },
                [Validators.required, Validators.email]
            ],
            password: [
                { value: '', disabled: this.showSpinner },
                Validators.required
            ]
        });
    }

    ngOnInit() {
    }

    showAlert(type: string) {
        if (type === 'success') {
            this.showSuccessAlert = true;
            setTimeout(() => {
                this.showSuccessAlert = false;
            }, 2000);
        } else {
            this.showErrorAlert = true;
            setTimeout(() => {
                this.showErrorAlert = false;
            }, 2000);
        }
    }

    formSubmit() {
        this.showSpinner = true;
        const subscription = this.apiService.userSignup(this.signupForm.value)
            .subscribe((tokens: AccessTokenServerResponse) => {
                this.showAlert('success');
                subscription.unsubscribe();
                this.showSpinner = false;
                const user: User = this.apiService.saveTokens(tokens);
                this.store.dispatch(new authActions.LoginSuccess({ user: user }));
                this.signupForm.reset();
                this.router.navigate(['home']);
            }, error => {
                try {
                    this.errorMessage = error['error']['message'] || 'Error creating user. Please try again.';
                } catch (err) {
                    this.errorMessage = 'Error creating user. Please try again.';
                }
                this.showAlert('error');
                subscription.unsubscribe();
                this.showSpinner = false;
                this.signupForm.reset();
                this.store.dispatch(new authActions.LoginFailure(error));
            });
    }

}
