import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import * as _ from 'lodash';

import {
    Idea,
    IdeaParams
} from '../../interfaces';

import { ApiService } from '../../services';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
    providers: [ApiService]
})
export class HomeComponent implements OnInit {

    public idea: Idea = {
        content: '',
        impact: 10,
        ease: 10,
        confidence: 10,
        edit: true,
        showAction: false,
        initial: true
    };

    public showTemplateIdea = false;

    public ideasList: Idea[] = [];

    public headings = [
        { title: '', name: 'content', span: 5, class: 'content-header' },
        { title: 'Impact', name: 'impact', span: 1, class: 'select-header' },
        { title: 'Ease', name: 'ease', span: 1, class: 'select-header' },
        { title: 'Confidence', name: 'confidence', span: 1, class: 'select-header' },
        { title: 'Avg.', name: 'average_score', span: 1, class: 'average-header' },
        { title: '', name: 'action', span: 2, class: 'actions-header' }
    ];

    public dropdownValues = _.range(1, 11);

    public page = 1;

    public pages = [1];

    constructor(
        private apiService: ApiService,
        public dialog: MatDialog
    ) { }

    ngOnInit() {
        this.getIdeasListFromServer();
        this.getIdeasListCountFromServer();
    }

    sortIdeasList() {
        this.ideasList = _.sortBy(this.ideasList, d => d.average_score).reverse();
    }

    getIdeasListFromServer() {
        const subscription = this.apiService.getIdeas(this.page)
        .subscribe((result: Idea[]) => {
            subscription.unsubscribe();
            this.ideasList = result.map(d => {
                d.edit = false;
                d.showAction = false;
                return d;
            });
            this.sortIdeasList();
        });
    }

    getIdeasListCountFromServer() {
        const subscription = this.apiService.getIdeasCount()
            .subscribe((result: object) => {
                subscription.unsubscribe();
                const pages = Math.ceil(result['count'] / 10) + 1;
                this.pages = _.range(1, pages);
            });
    }

    getPage(page: any) {
        const original = this.page;
        if (page === 'previous') {
            const p = this.page - 1;
            this.page = p < 1 ? 1 : p;
        } else if (page === 'next') {
            const p = this.page + 1;
            this.page = this.pages.indexOf(p) !== -1 ? p : this.page;
        } else {
            this.page = this.pages.indexOf(page) !== -1 ? page : this.page;
        }
        if (this.page !== original) {
            this.getIdeasListFromServer();
        }
    }

    saveIdeaToServer(idea: Idea) {
        const data: IdeaParams = {
            content: idea.content,
            impact: idea.impact,
            ease: idea.ease,
            confidence: idea.confidence,
            average_score: idea.average_score
        };
        const subscription = this.apiService.createIdea(data)
            .subscribe((result: Idea) => {
                subscription.unsubscribe();
                idea.id = result.id;
                console.log(result);
            });
    }

    updateIdeaOnServer(idea: Idea) {
        const data: IdeaParams = {
            content: idea.content,
            impact: idea.impact,
            ease: idea.ease,
            confidence: idea.confidence,
            average_score: idea.average_score
        };
        const subscription = this.apiService.updateIdea(idea.id, data)
            .subscribe((result: Idea) => {
                subscription.unsubscribe();
                console.log(result);
            });
    }

    deleteIdeaOnServer(idea: Idea) {
        const subscription = this.apiService.deleteIdea(idea.id)
            .subscribe((result: any) => {
                subscription.unsubscribe();
                console.log(result);
            });
    }

    addIdea() {
        const idea = _.clone(this.idea);
        idea.tempvalues = {
            content: idea.content,
            impact: idea.impact,
            ease: idea.ease,
            confidence: idea.confidence,
            average_score: idea.average_score
        };
        this.ideasList.splice(0, 0, idea);
    }

    showActionButtons(row: Idea) {
        row.showAction = true;
    }

    hideActionButtons(row: Idea) {
        row.showAction = false;
    }

    editIdea(idea: Idea) {
        idea.edit = true;
        idea.tempvalues = {
            content: idea.content,
            impact: idea.impact,
            ease: idea.ease,
            confidence: idea.confidence,
            average_score: idea.average_score
        };
    }

    public calcAverage(idea: Idea) {
        const avg = (idea.impact + idea.ease + idea.confidence) / 3;
        return +avg.toFixed(2);
    }

    async confirmEdit(idea: Idea) {
        idea.edit = false;
        idea.average_score = this.calcAverage(idea);
        this.sortIdeasList();
        if (idea.initial) {
            delete idea.initial;
            await this.saveIdeaToServer(idea);
        } else {
            await this.updateIdeaOnServer(idea);
        }
        setTimeout(async () => {
            await this.getIdeasListFromServer();
            await this.getIdeasListCountFromServer();
        }, 1000);
    }

    async rejectEdit(idea: Idea) {
        if (!idea.initial) {
            idea.edit = false;
            idea.content = idea.tempvalues.content;
            idea.impact = idea.tempvalues.impact;
            idea.ease = idea.tempvalues.ease;
            idea.confidence = idea.tempvalues.confidence;
            idea.average_score = idea.tempvalues.average_score;
        } else {
            await this.deleteIdea(idea);
        }
        setTimeout(async () => {
            await this.getIdeasListFromServer();
            await this.getIdeasListCountFromServer();
        }, 1000);
    }

    async deleteIdea(idea: Idea) {
        const index = this.ideasList.indexOf(idea);
        this.ideasList.splice(index, 1);
        this.sortIdeasList();
        await this.deleteIdeaOnServer(idea);
        setTimeout(async () => {
            await this.getIdeasListFromServer();
            await this.getIdeasListCountFromServer();
        }, 1000);
    }

    openDialog(row: Idea): void {
        const dialogRef = this.dialog.open(DeleteDialogComponent, {
            width: '450px',
            data: row
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.deleteIdea(result);
            }
        });
    }

}

@Component({
    selector: 'app-confirm-delete',
    template: `
        <h1 mat-dialog-title>Are you sure?</h1>
        <div mat-dialog-content>
            <p>This idea will be permanently deleted.</p>
        </div>
        <div mat-dialog-actions>
            <button mat-button (click)="onNoClick()">CANCEL</button>
            <button class="mat-dialog-ok" mat-button [mat-dialog-close]="data" cdkFocusInitial>OK</button>
        </div>
    `,
    styles: [
        '.mat-dialog-title { text-align: center; }',
        '.mat-dialog-content { text-align: center; }',
        '.mat-dialog-actions { margin: auto; width: 12em; }',
        '.mat-dialog-ok { color: #00a843; }'
    ]
})
export class DeleteDialogComponent {

    constructor(
        public dialogRef: MatDialogRef<DeleteDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: Idea) { }

    onNoClick(): void {
        this.dialogRef.close(false);
    }
}
