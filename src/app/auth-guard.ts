import { Injectable } from '@angular/core';
import {
    CanActivate, Router,
    ActivatedRouteSnapshot,
    RouterStateSnapshot
} from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';

import { Store } from '@ngrx/store';
import * as fromRoot from './ngrx/reducers';
import * as authActions from './ngrx/actions/auth';

import { ApiService } from './services';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(
        private jwtHelper: JwtHelperService,
        private router: Router,
        private apiService: ApiService,
        private store: Store<fromRoot.State>
    ) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const url: string = state.url;

        // return true;
        const expired = this.jwtHelper.isTokenExpired();

        if (expired) {
            // this.apiService.deleteTokens();
            // this.store.dispatch(new authActions.Logout());
            // this.router.navigate(['login']);
            this.apiService.logout();
            return false;
        } else {
            this.store.dispatch(new authActions.LoggedIn());
            return true;
        }
    }
}
