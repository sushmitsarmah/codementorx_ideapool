import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {
  MatFormFieldModule,
  MatInputModule,
  MatButtonModule,
  MatDialogModule,
  MatProgressSpinnerModule
} from '@angular/material';

import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';

import { NgxPaginationModule } from 'ngx-pagination';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { APP_BASE_HREF } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';

import { JwtModule } from '@auth0/angular-jwt';

import { AuthGuard } from './auth-guard';

import { httpInterceptorProviders } from './api.interceptor';

import { AppRouterModule } from './app.router';

import { AppComponent } from './app.component';

import { ApiService } from './services';

import { reducers } from './ngrx/reducers';

import {
    LoginComponent,
    SignupComponent,
    HomeComponent,
    DeleteDialogComponent,
    PageNotFoundComponent
 } from './views';

import {
    SidebarComponent
} from './components';

export function tokenGetter() {
  return localStorage.getItem('cmx_access_token');
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    HomeComponent,
    DeleteDialogComponent,
    PageNotFoundComponent,
    SidebarComponent
  ],
  imports: [
    BrowserModule,

    BrowserAnimationsModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatDialogModule,
    MatProgressSpinnerModule,

    NgbDropdownModule,

    AppRouterModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    StoreModule.forRoot(reducers),

    NgxPaginationModule,

    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        headerName: 'x-access-token',
        authScheme: '',
        whitelistedDomains: [],
        blacklistedRoutes: []
      }
    })
  ],
  entryComponents: [DeleteDialogComponent],
  providers: [
    { provide: APP_BASE_HREF, useValue: '/' },
    AuthGuard,
    httpInterceptorProviders,
    ApiService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
