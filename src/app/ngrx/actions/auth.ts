import { Action } from '@ngrx/store';
import { User, LoginParams } from '../../interfaces';

export enum AuthActionTypes {
    Login = '[Auth] Login',
    Logout = '[Auth] Logout',
    LoggedIn = '[Auth] Logged In',
    LoginSuccess = '[Auth] Login Success',
    LoginFailure = '[Auth] Login Failure',
    LoginRedirect = '[Auth] Login Redirect',
}

export class Login implements Action {
    readonly type = AuthActionTypes.Login;

    constructor(public payload: LoginParams) { }
}

export class LoginSuccess implements Action {
    readonly type = AuthActionTypes.LoginSuccess;

    constructor(public payload: { user: User }) { }
}

export class LoginFailure implements Action {
    readonly type = AuthActionTypes.LoginFailure;

    constructor(public payload: any) { }
}

export class LoggedIn implements Action {
    readonly type = AuthActionTypes.LoggedIn;
}

export class Logout implements Action {
    readonly type = AuthActionTypes.Logout;
}

export type AuthActionsUnion =
    | Login
    | LoginSuccess
    | LoginFailure
    | LoggedIn
    | Logout;
