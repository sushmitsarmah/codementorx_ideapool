import {
    ActionReducerMap,
    createSelector,
    createFeatureSelector,
    ActionReducer,
    // MetaReducer,
} from '@ngrx/store';

/**
 * Every reducer module's default export is the reducer function itself. In
 * addition, each module should export a type or interface that describes
 * the state of the reducer plus any selector functions. The `* as`
 * notation packages up all of the exports into a single object.
 */

import * as fromAuth from './auth';
import * as fromLoginPage from './login-page.auth';

/**
 * As mentioned, we treat each reducer like a table in a database. This means
 * our top level state interface is just a map of keys to inner state types.
 */
export interface State {
    auth: fromAuth.State;
    loginAuth: fromLoginPage.State;
}

/**
 * Our state is composed of a map of action reducer functions.
 * These reducer functions are called with each dispatched action
 * and the current or initial state and return a new immutable state.
 */
export const reducers: ActionReducerMap<State> = {
    auth: fromAuth.reducer,
    loginAuth: fromLoginPage.reducer
};

// console.log all actions
export function logger(reducer: ActionReducer<State>): ActionReducer<State> {
    return function (state: State, action: any): State {
        console.log('state', state);
        console.log('action', action);

        return reducer(state, action);
    };
}

/**
 * By default, @ngrx/store uses combineReducers with the reducer map to compose
 * the root meta-reducer. To add more meta-reducers, provide an array of meta-reducers
 * that will be composed to form the root meta-reducer.
 */
// export const metaReducers: MetaReducer<State>[] = !environment.production
//     ? [logger]
//     : [];

/**
 * Auth Reducers
 */
export const getAuthState = createFeatureSelector<State, fromAuth.State>(
    'auth'
);

export const getLoggedIn = createSelector(
    getAuthState,
    fromAuth.getLoggedIn
);

export const getUser = createSelector(
    getAuthState,
    fromAuth.getUser
);

/**
 * Login Page Reducers
 */
export const getLoginAuthState = createFeatureSelector<State, fromLoginPage.State>(
    'loginAuth'
);

export const getLoginError = createSelector(
    getLoginAuthState,
    fromLoginPage.getError
);

export const getPending = createSelector(
    getLoginAuthState,
    fromLoginPage.getPending
);

export const getLoginParams = createSelector(
    getLoginAuthState,
    fromLoginPage.getLoginParams
);

export const getLoginStates = createSelector(
    getLoginAuthState,
    fromLoginPage.getLoginStates
);
