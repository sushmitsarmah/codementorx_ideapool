import { AuthActionTypes, AuthActionsUnion } from '../actions/auth';
import { createSelector } from 'reselect';
import { LoginParams } from '../../interfaces';

export interface State {
    error: string | null;
    pending: boolean;
    params: LoginParams;
}

export const initialState: State = {
    error: null,
    pending: false,
    params: <LoginParams>{}
};

export function reducer(state = initialState, action: AuthActionsUnion): State {
    switch (action.type) {
        case AuthActionTypes.Login: {
            return {
                ...state,
                error: null,
                pending: true,
                params: action.payload
            };
        }

        case AuthActionTypes.LoginSuccess: {
            return {
                ...state,
                error: null,
                pending: false,
                params: <LoginParams>{}
            };
        }

        case AuthActionTypes.LoginFailure: {
            return {
                ...state,
                error: action.payload,
                pending: false,
                params: <LoginParams>{}
            };
        }

        default: {
            return state;
        }
    }
}

export const getError = (state: State) => state.error;
export const getPending = (state: State) => state.pending;
export const getLoginParams = (state: State) => state.params;

export const getLoginStates = createSelector(
    getError,
    getPending,
    getLoginParams,
    (error, pending, params) => {
        return {
            error: error,
            pending: pending,
            params: params
        };
    });
