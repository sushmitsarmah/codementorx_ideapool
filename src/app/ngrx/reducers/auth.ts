import { AuthActionsUnion, AuthActionTypes } from '../actions/auth';
import { User } from '../../interfaces';

export interface State {
    loggedIn: boolean;
    user: User | null;
}

export const initialState: State = {
    loggedIn: false,
    user: null,
};

export function reducer(state = initialState, action: AuthActionsUnion): State {
    switch (action.type) {
        case AuthActionTypes.LoginSuccess: {
            return {
                ...state,
                loggedIn: true,
                user: action.payload.user,
            };
        }

        case AuthActionTypes.LoggedIn: {
            return {
                ...state,
                loggedIn: true
            };
        }

        case AuthActionTypes.Logout: {
            return initialState;
        }

        default: {
            return state;
        }
    }
}

export const getLoggedIn = (state: State) => state.loggedIn;
export const getUser = (state: State) => state.user;
