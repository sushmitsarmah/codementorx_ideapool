import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import AppRouterModule from './app.router';

import { ApiService } from './services';

import {
  LoginComponent,
  SignupComponent,
  HomeComponent,
  PageNotFoundComponent
} from './views';

import {
  SidebarComponent
} from './components';

// describe('AppComponent', () => {
//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       declarations: [
//         AppComponent,
//         LoginComponent,
//         SignupComponent,
//         HomeComponent,
//         PageNotFoundComponent,
//         SidebarComponent
//       ],
//       imports: [
//         AppRouterModule
//       ],
//       providers: [
//         ApiService
//       ],
//     }).compileComponents();
//   }));
//   it('should create the app', async(() => {
//     const fixture = TestBed.createComponent(AppComponent);
//     const app = fixture.debugElement.componentInstance;
//     expect(app).toBeTruthy();
//   }));
//   // it(`should have as title 'ideapool'`, async(() => {
//   //   const fixture = TestBed.createComponent(AppComponent);
//   //   const app = fixture.debugElement.componentInstance;
//   //   expect(app.title).toEqual('ideapool');
//   // }));
//   // it('should render title in a h1 tag', async(() => {
//   //   const fixture = TestBed.createComponent(AppComponent);
//   //   fixture.detectChanges();
//   //   const compiled = fixture.debugElement.nativeElement;
//   //   expect(compiled.querySelector('h1').textContent).toContain('Welcome to ideapool!');
//   // }));
// });
