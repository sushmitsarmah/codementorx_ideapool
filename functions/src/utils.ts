import * as express from 'express';
import * as bcrypt from 'bcryptjs';
import * as jwt from 'jsonwebtoken';
import * as randToken from 'rand-token';

import { LoginError } from './interfaces';

import { dbFunctions as db } from './db';

const salt = bcrypt.genSaltSync(10);
const secret = "?8U.xHDt'w!b(n]!X5";
const expirationTime = 600; //in seconds. expires in 10 minutes

// const refreshTokens = {};

// encrypt password
export const encryptPassword = (password: string): string => {
    const hashedPassword = bcrypt.hashSync(password, salt);
    return hashedPassword;
};

// compare passwords
export const comparePassword = (password: string, hashedPassword: string): Promise<boolean> => {
    return bcrypt.compare(password, hashedPassword);
};

// generate a jwt
export const genJWT = (details) => {
    const tokenParams = {
        email: details.email,
        name: details.name,
        id: details.id
    };
    const token = jwt.sign(tokenParams, secret, {
        expiresIn: expirationTime
    });
    return token;
};

export const genRefreshToken = (email) => {
    const refreshToken = randToken['uid'](256);

    return db.addRFToken(email, refreshToken)
    .then( result => {
        return refreshToken;
    });   
};

export const deleteRefreshToken = (refreshToken) => {
    return db.deleteRFToken(refreshToken)
    .then( d => {
        console.log(`refresh token deleted`);
        return d;
    });
};

// verify that jwt is valid
export const verifyJWT = (token, cb) => {
    jwt.verify(token, secret, (err, decoded) => {
        cb(err, decoded);
    });
};

export const verifyRefreshToken = (refreshToken, details) => {

    return db.getRFToken(refreshToken)
    .then( d => {
        if (d && d.email === details.email){
            const token = genJWT(details);
            return token;
        } else {
            return false;
        }
    })
    .then( (d: string | boolean) => {
        return d;
    });
};

// verify login
export const verifyLogin = (email, password): Promise<LoginError | object> => {
    return db.getUserByEmail(email)
    .then( user => {
        if(user){
            const cmp = comparePassword(password, user.password)
            .then( pmatch => {
                if(pmatch) {
                    const details = {
                        email: user.email,
                        name: user.name,
                        id: user.id
                    };
                    const token = genJWT(details);
                    return {
                        user: details,
                        jwt: token,
                        error: false
                    }
                } else {
                    return {
                        error: true,
                        message: 'email/password did not match'
                    }
                }
            });
            return Promise.resolve(cmp);
        } else {
            return {
                error: true,
                message: 'email/password did not match'
            }
        }
    });
};

// authenticates paths by checking for access token
export const authenticatePath = (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
) => {
    const access_token = req.headers['x-access-token'];
    verifyJWT(access_token, (err, decoded)=>{
        if (err){
            res.status(401).json({ auth: false, message: 'Failed to authenticate token.' });
        } else {
            req['user'] = decoded;
            next();
        }
    });
};

export const decodeJWT = (token: string) => {
    return jwt.decode(token);
};

