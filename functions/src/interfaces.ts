export interface LoginParams {
    email: string;
    password: string;
}

export interface SignupParams {
    email: string;
    password: string;
    name: string;
}

export interface AccessTokenServerResponse {
    jwt: string;
    refresh_token?: string;
}

export interface LoginError {
    error: boolean;
    message: string;
}

export interface User {
    id?: string;
    name: string;
    email: string;
    password?: string;
    avatar_url?: string;
    created_at?: any;
    updated_at?: any;
}

export interface Idea {
    id?: string;
    content: string;
    impact: number;
    ease: number;
    confidence: number;
    average_score?: number;
    created_at?: any;
    updated_at?: any;
}


