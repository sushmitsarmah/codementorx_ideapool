import * as functions from 'firebase-functions';

import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as cookieParser from 'cookie-parser';

import * as cors from 'cors';

import { appRoutes } from './app';

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors());

app.use('/api/v1', appRoutes());

export const api = functions.https.onRequest(app);