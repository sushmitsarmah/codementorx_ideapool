import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';
import * as gravatar from 'gravatar';
import * as _ from 'lodash';

import { Idea, User} from './interfaces';

admin.initializeApp(functions.config().firebase);
const db = admin.firestore();

const usersCollection = 'users';
const ideasCollection = 'ideas';
const rfTokenCollection = 'refreshTokens';

const getUserByEmail = (email: string): Promise<any> => {
    return db.collection(usersCollection).where('email', '==', email).get()
        .then((snapshot: admin.firestore.QuerySnapshot) => {
            const doc = snapshot.docs[0];
            if (doc) {
                const user = doc.data();
                const result = {
                    email: user.email,
                    name: user.name,
                    id: doc.id,
                    password: user.password
                }
                return result;
            } else {
                return undefined;
            }
        });
};

const getUserById = (id: string): Promise<User> => {
    return db.collection(usersCollection).doc(id).get()
        .then(doc => {
            if (!doc.exists) {
                console.log('No such document!');
                return <User>{};
            } else {
                const user: User = <User>doc.data();
                const result: User = {
                    id: id,
                    email: user.email,
                    name: user.name,
                    avatar_url: user.avatar_url
                };
                return result;
            }
        });
};

const addUser = async (userObj: User): Promise<User> => {
    const timestamp = Date.now();//admin.firestore.FieldValue.serverTimestamp();
    userObj['created_at'] = timestamp;
    userObj['avatar_url'] = gravatar.url(userObj.email, { s: '100', r: 'x', d: 'retro' }, true);
    const returnedUser = await getUserByEmail(userObj.email);
    if (returnedUser) {
        return Promise.resolve(<User>{});
    } else {
        return db.collection(usersCollection).add(userObj)
            .then((docRef: admin.firestore.DocumentReference) => {
                userObj['id'] = docRef.id;
                const result: User = {
                    id: docRef.id,
                    email: userObj.email,
                    name: userObj.name
                };
                return result;
            });            
    }
};

const addRFToken = (email: string, rftoken: string): Promise<any> => {
    const obj = {
        email: email
    };

    return db.collection(rfTokenCollection).doc(rftoken).set(obj)
        .then((writeResult: any) => {
            return {
                done: true
            };
        });
};

const getRFToken = (rftoken: string): Promise<any> => {
    return db.collection(rfTokenCollection)
        .doc(rftoken).get()
        .then((snapshot: admin.firestore.DocumentSnapshot) => {
            return snapshot.data();
        });
};

const deleteRFToken = (rftoken: string): Promise<any> => {
    return db.collection(rfTokenCollection).doc(rftoken).delete();
};

const addIdea = (ideaObj: Idea, userid: string): Promise<Idea> => {
    const timestamp = Date.now();//admin.firestore.FieldValue.serverTimestamp();
    ideaObj['created_at'] = timestamp;
    ideaObj['userid'] = userid;
    // ideaObj['average_score'] = (ideaObj.confidence + ideaObj.impact + ideaObj.ease)/3;
    return db.collection(ideasCollection).add(ideaObj)
        .then((docRef: admin.firestore.DocumentReference) => {
            const result: Idea = {
                id: docRef.id,
                content: ideaObj.content,
                impact: ideaObj.impact,
                ease: ideaObj.ease,
                confidence: ideaObj.confidence,
                average_score: ideaObj.average_score,
                created_at: timestamp
            };
            return result;
        });
};

const updateIdea = (ideaObj: Idea, userid: string): Promise<Idea> => {
    const timestamp = Date.now();//admin.firestore.FieldValue.serverTimestamp();
    ideaObj['updated_at'] = timestamp;
    return db.collection(ideasCollection).doc(ideaObj.id).update(ideaObj)
        .then((writeResult: any) => {
            const result: Idea = {
                id: ideaObj.id,
                content: ideaObj.content,
                impact: ideaObj.impact,
                ease: ideaObj.ease,
                confidence: ideaObj.confidence,
                average_score: ideaObj.average_score,
                created_at: ideaObj.created_at
            };
            return result;
        });
};

const getIdeas = (page: number, userid: string): Promise<Idea[]> => {
    return db.collection(ideasCollection)
        .where('userid', '==', userid)
        .get()
        .then( (snapshot: admin.firestore.QuerySnapshot) => {
            let ideas: Idea[] = snapshot.docs.map(doc => {
                const idea: Idea = <Idea>doc.data();
                const result: Idea = {
                    id: doc.id,
                    content: idea.content,
                    impact: idea.impact,
                    ease: idea.ease,
                    confidence: idea.confidence,
                    average_score: idea.average_score,
                    created_at: idea.created_at
                };
                return result;                
            });
            ideas = _.sortBy(ideas, d => d.average_score).reverse();
            if(page === 0) {
                return ideas;
            } else {
                const lowerLimit = (page-1) * 10;
                const upperLimit = lowerLimit + 10;
                return ideas.slice(lowerLimit, upperLimit);
            }
        });
};

const deleteIdea = (id: string, userid: string): Promise<any> => {
    return db.collection(ideasCollection).doc(id).delete();
};

export const dbFunctions = {
    addUser: addUser,
    addIdea: addIdea,
    getIdeas: getIdeas,
    updateIdea: updateIdea,
    deleteIdea: deleteIdea,
    getUserById: getUserById,
    getUserByEmail: getUserByEmail,
    addRFToken: addRFToken,
    getRFToken: getRFToken,
    deleteRFToken: deleteRFToken
};
