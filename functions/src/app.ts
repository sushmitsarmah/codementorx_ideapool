import * as express from 'express';
import {
    Idea,
    User,
    AccessTokenServerResponse,
    LoginError
} from './interfaces';

import { dbFunctions as db } from './db';
import * as utils from './utils';

export const appRoutes = (): express.Express => {
    const app = express();

    app.get('/', (req: express.Request, res: express.Response) => {
        res.json({api: 'v1'});
    });

    // refresh access token with given refresh_token
    const accessTokenRefresh = (req: express.Request, res: express.Response) => {
        const refresh_token = req.body.refresh_token;
        const access_token = <string>req.headers['x-access-token'];
        const details = utils.decodeJWT(access_token);

        utils.verifyRefreshToken(refresh_token, details)
        .then( jwt => {
            if (jwt) {
                res.status(200).json({ jwt: jwt });
            } else {
                res.status(401).json({ message: 'Invalid Refresh Token' });
            }
        })
        .catch(error => {
            console.log(error);
            res.status(500).json({
                message: error,
            });
        });
    };

    // get access token on login
    const getAccessToken = (req: express.Request, res: express.Response) => {
        const email = req.body.email;
        const password = req.body.password;

        utils.verifyLogin(email, password)
        .then((obj: object | LoginError) => {
            if (obj['error']){
                res.status(401).json(<LoginError>obj);
            } else {
                req['user'] = obj['details'];

                utils.genRefreshToken(email)
                .then(refreshToken => {
                    const tokenResponse = {
                        jwt: obj['jwt'],
                        refresh_token: refreshToken
                    };
                    res.status(201).json(<AccessTokenServerResponse>tokenResponse);
                })
                .catch(error => {
                    console.log(error);
                    res.status(500).json({
                        message: error,
                    });
                });
            }
        })
        .catch( error => {
            console.error("Error logging in: ", error);
            res.status(500).json({
                message: error,
            });
        })
    };

    // delete the access token
    const deleteAccessToken = (req: express.Request, res: express.Response) => {
        const refresh_token = req.body.refresh_token || req.query.refresh_token || '';
        utils.deleteRefreshToken(refresh_token)
        .then( d => {
            res.status(204).json({ message: "access token deleted" });
        })
        .catch(error => {
            console.log(error);
            res.status(500).json({
                message: error
            });
        });
    };

    // get the current user
    const getCurrentUser = (req: express.Request, res: express.Response) => {
        const id = req['user']['id'];
        db.getUserById(id)
        .then( (user: User) => {
            res.status(200).json(user);
        })
        .catch(error => {
            console.error("Error getting current user: ", error);
            res.status(500).json({
                message: error,
            });
        });
    };

    // create a new idea
    const createIdea = (req: express.Request, res: express.Response) => {
        const content = req.body.content;
        const impact = req.body.impact;
        const ease = req.body.ease;
        const confidence = req.body.confidence;
        const average_score = req.body.average_score;

        const userid = req['user']['id'];

        const idea: Idea = {
            content: content,
            impact: impact,
            ease: ease,
            confidence: confidence,
            average_score: average_score
        };

        db.addIdea(idea, userid)
        .then( (result: Idea) => {
            res.status(201).json(result);
        })
        .catch(error => {
            console.error("Error adding idea: ", error);
            res.status(500).json({
                message: error,
            });
        });
    };
    
    // get a page of ideas
    const getIdeas = (req: express.Request, res: express.Response) => {
        const page = +req.query.page || 1;

        const userid = req['user']['id'];

        db.getIdeas(page, userid)
        .then( (ideas: Idea[]) => {
            res.status(200).json(ideas);
        })
        .catch(error => {
            console.error("Error getting ideas list: ", error);
            res.status(500).json({
                message: error,
            });
        });
    };

    const getIdeasCount = (req: express.Request, res: express.Response) => {
        const userid = req['user']['id'];

        db.getIdeas(0, userid)
            .then((ideas: Idea[]) => {
                res.status(200).json({ count: ideas.length });
            })
            .catch(error => {
                console.error("Error getting ideas list: ", error);
                res.status(500).json({
                    message: error,
                });
            });        
    };
    
    // delete an idea
    const deleteIdea = (req: express.Request, res: express.Response) => {
        const id = req.params.id;

        const userid = req['user']['id'];

        db.deleteIdea(id, userid)
        .then(result => {
            res.status(204).json({ message: "idea deleted" });
        })
        .catch(error => {
            console.error("Error deleting idea: ", error);
            res.status(500).json({
                message: error,
            });
        });
    };
    
    // update an idea
    const updateIdea = (req: express.Request, res: express.Response) => {
        const id = req.params.id;
        const content = req.body.content;
        const impact = req.body.impact;
        const ease = req.body.ease;
        const confidence = req.body.confidence;
        const average_score = req.body.average_score;

        const userid = req['user']['id'];

        const ideaObj: Idea = {
            id: id,
            content: content,
            impact: impact,
            ease: ease,
            confidence: confidence,
            average_score: average_score
        };

        db.updateIdea(ideaObj, userid)
        .then(idea => {
            res.status(200).json(idea);
        })
        .catch(error => {
            console.error("Error updating idea: ", error);
            res.status(500).json({
                message: error,
            });
        });
    };

    // signup new users
    const userSignup = (req: express.Request, res: express.Response) => {
        const email = req.body.email;
        const password = req.body.password;
        const name = req.body.name;
        

        db.addUser({
            name: name,
            email: email,
            password: utils.encryptPassword(password)
        })
        .then( (user: User) => {
            if(user.email){
                const details = {
                    email: user.email,
                    name: user.name,
                    id: user.id
                };
                const jwt = utils.genJWT(details);
                utils.genRefreshToken(user.email)
                .then(refresh_token => {
                    req['user'] = details;

                    res.status(201).json({
                        jwt: jwt,
                        refresh_token: refresh_token
                    }); 
                })
                .catch(error => {
                    console.log(error);
                    res.status(500).json({
                        message: error,
                    });
                });
            } else {
                res.status(500).json({
                    message: 'user email already exists'
                }); 
            }
        })
        .catch(error => {
            console.error("Error adding user: ", error);
            res.status(500).json({
                message: error,
            }); 
        });
    };

    /**************** Routes ***************/

    // signup and get token
    app.post('/users', userSignup);

    // login and get token
    app.post('/access-tokens', getAccessToken);

    // refresh access token
    app.post('/access-tokens/refresh', accessTokenRefresh);

    // delete access token
    app.delete('/access-tokens', deleteAccessToken);    

    // authenticate all paths from here
    app.use(utils.authenticatePath);

    // get the current user
    app.get('/me', getCurrentUser);

    // ideas routes
    app.get('/ideas', getIdeas);
    app.get('/total-ideas', getIdeasCount);
    app.post('/ideas', createIdea);
    app.delete('/ideas/:id', deleteIdea);
    app.put('/ideas/:id', updateIdea);

    return app;
};